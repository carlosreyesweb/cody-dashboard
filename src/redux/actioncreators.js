import * as constants from '../constants/redux';

export function setLoginState() {
  return { type: constants.LOGIN, payload: true };
}

export function setLogoutState() {
  return { type: constants.LOGOUT, payload: false };
}

export function setUserData(payload) {
  return {
    type: constants.SET_USER_DATA,
    payload,
  };
}

export function removeUserData() {
  return { type: constants.REMOVE_USER_DATA, payload: {} };
}

export function setProductsData(payload) {
  return {
    type: constants.SET_PRODUCTS_DATA,
    payload,
  };
}

export function removeProductsData() {
  return { type: constants.REMOVE_PRODUCTS_DATA, payload: [] };
}
