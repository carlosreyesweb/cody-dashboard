import {
  loginStateReducer,
  productsDataReducer,
  userDataReducer,
} from './reducers';
import { createStore, combineReducers } from 'redux';

const store = createStore(
  combineReducers({
    userLoginStatus: loginStateReducer,
    productsData: productsDataReducer,
    userData: userDataReducer,
  })
);

export default store;
