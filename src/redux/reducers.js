import * as constants from '../constants/redux';

export function loginStateReducer(state = false, action) {
  switch (action.type) {
    case constants.LOGIN:
      return action.payload;
    case constants.LOGOUT:
      return action.payload;
    default:
      return state;
  }
}

export function userDataReducer(state = {}, action) {
  switch (action.type) {
    case constants.SET_USER_DATA:
      return { ...state, ...action.payload };
    case constants.REMOVE_USER_DATA:
      return action.payload;
    default:
      return state;
  }
}

export function productsDataReducer(state = [], action) {
  switch (action.type) {
    case constants.SET_PRODUCTS_DATA:
      return [...new Set(action.payload)];
    case constants.REMOVE_PRODUCTS_DATA:
      return action.payload;
    default:
      return state;
  }
}
