import React from 'react';
//Utils
import PropTypes from 'prop-types';
//Styles
import './SectionLayout.scss';

export default function SectionLayout({ children, optClass }) {
  return <div className={`section ${optClass}`}>{children}</div>;
}

SectionLayout.propTypes = {
  optClass: PropTypes.string,
};
