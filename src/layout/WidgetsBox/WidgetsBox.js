import React from 'react';
import PropTypes from 'prop-types';
//Styles
import './WidgetsBox.scss';

export default function WidgetsBox({ children, layout }) {
  return <div className={`widgets-box--${layout}`}>{children}</div>;
}

WidgetsBox.propTypes = {
  layout: PropTypes.oneOf(['graphs', 'products', 'profile']),
};
