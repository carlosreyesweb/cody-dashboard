import React from 'react';
//Components
import LoginPage from './components/LoginPage/LoginPage';
import DashboardPage from './components/DashboardPage/DashboardPage';
//Utils
import { useSelector } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
//Styles
import './styles/App.scss';

function App() {
  const userLoggedIn = useSelector(state => state.userLoginStatus);

  return (
    <Router>
      <Route exact path="/">
        <LoginPage />
      </Route>
      <Route path="/dashboard">
        {userLoggedIn ? <DashboardPage /> : <Redirect to="/" />}
      </Route>
    </Router>
  );
}

export default App;
