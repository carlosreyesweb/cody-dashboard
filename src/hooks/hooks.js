import { useState, useLayoutEffect } from 'react';
import { useSelector } from 'react-redux';

/**
 * Injects responsive mobile menu behavior for Menu components.
 *
 * @param {number} widthBreakPoint - Window width breakpoint. It's used
 * to handle mobile and desktop behaviors based on window width, e.g if you
 * only want to have an hamburger-like menu behavior in mobile and a navbar
 * behavior in desktop using the same component.
 * @returns A boolean flag state variable and a toggle function. Boolean flag
 * can be used to determine e.g if a menu is shown or hidden. Toggle function
 * can be used e.g in open and close buttons.
 *
 * @example
 * function Menu() {
 *   const [isMenuOpen, showHideMenu] = useInteractiveMenu(768)
 *
 *   return (
 *     <>
 *       <button onClick={showHideMenu}>
 *         {isMenuOpen ? 'CLOSE MENU' : 'OPEN MENU'}
 *       </button>
 *       <nav className={`menu--${isMenuOpen ? 'shown' : 'hidden'}`}>
 *         <ul>
 *           <li>ITEM</li>
 *           <li>ITEM</li>
 *           <li>ITEM</li>
 *         </ul>
 *       </nav>
 *     </>
 *   )
 * }
 */
export function useInteractiveMenu(widthBreakPoint) {
  const [isOpen, setIsOpen] = useState(false);

  function toggleShowMenu() {
    if (window.outerWidth < widthBreakPoint) {
      setIsOpen(state => !state);
    }
  }

  function detectWindowWidth() {
    if (window.outerWidth >= widthBreakPoint) {
      setIsOpen(true);
    }
  }

  useLayoutEffect(() => {
    detectWindowWidth();
    window.addEventListener('resize', detectWindowWidth);
    return () => window.removeEventListener('resize', detectWindowWidth);
  });

  return [isOpen, toggleShowMenu];
}

/**
 * Input capture hook.
 *
 * @param {object} initialState - An object whose properties could match
 * input "name" attributes from each input field in a form. You
 * can pass an empty object if your form does not have initial data.
 * @return An object with input "name" attributes as properties
 * and values from each input, and a function to update them.
 *
 * @example
 * function Form() {
 *   const [formData, handleInputChange] = useInputHandler({})
 *
 *   console.log(formData) // See how data is being captured with this hook.
 *
 *   async function fancySubmit(event) {
 *     event.preventDefault()
 *     // Note how formData properties matches with every input 'name' attribute.
 *     const res = await API.fancyLogin(formData.email, formData.password)
 *   }
 *
 *   return (
 *     <form onSubmit={fancySubmit}>
 *       <input name="email" onChange={handleInputChange} />
 *       <input name="password" onChange={handleInputChange} />
 *     </form>
 *   )
 * }
 */
export function useInputHandler(initialState) {
  const [formData, setFormData] = useState(initialState);

  function handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    setFormData(state => {
      return { ...state, [name]: value };
    });
  }

  return [formData, handleChange];
}

/**
 * Hook to filter and reduce codyUses data.
 *
 * @param {string} data - String that specifies which data will be used.
 * only 'category' and 'date' are valid arguments.
 * @param {string} legend - For some charts, this param determines the
 * legend of the dataset.
 * @returns A chart-ready data object if codyUses in db.json is not an empty
 * array. Otherwise, function will return null.
 */
export function useUsesData(data, legend) {
  const usesArray = useSelector(state => state.userData.codyUses).map(use => ({
    uses: use.uses,
    [data]: use[data],
  }));

  if (usesArray.length !== 0) {
    const curatedUsesArray = [];
    usesArray.forEach(function (use) {
      if (curatedUsesArray.length === 0) {
        curatedUsesArray.push(use);
      } else {
        if (curatedUsesArray.some(item => item[data] === use[data])) {
          const duplicatedUse = curatedUsesArray.findIndex(
            item => item[data] === use[data]
          );
          curatedUsesArray[duplicatedUse].uses += use.uses;
        } else {
          curatedUsesArray.push(use);
        }
      }
    });

    return {
      labels: curatedUsesArray.map(use => use[data]),
      datasets: [
        {
          data: curatedUsesArray.map(use => use.uses),
          label: legend,
        },
      ],
    };
  } else {
    return null;
  }
}
