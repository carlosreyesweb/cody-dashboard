import React from 'react';
//Utils
import PropTypes from 'prop-types';
//Styles
import './Spinner.scss';

const Spinner = ({ color, position, offset }) => (
  <div className={`spinner ${color}`} style={{ position, ...offset }} />
);

export default Spinner;

Spinner.defaultProps = {
  color: 'black',
  position: 'static',
};

Spinner.propTypes = {
  color: PropTypes.string,
  position: PropTypes.oneOf(['absolute', 'fixed', 'static']),
  offset: PropTypes.exact({
    top: PropTypes.string,
    left: PropTypes.string,
    right: PropTypes.string,
    bottom: PropTypes.string,
  }),
};
