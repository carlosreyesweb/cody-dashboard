import React, { useEffect } from 'react';
//Components
import SectionLayout from '../../layout/SectionLayout/SectionLayout';
import SectionTitle from '../SectionTitle/SectionTitle';
import WidgetsBox from '../../layout/WidgetsBox/WidgetsBox';
import Product from '../Product/Product';
import Spinner from '../Spinner/Spinner';
import { toast as showToast, question } from '../Alerts/Alerts';
//Utils
import { useTranslation } from 'react-i18next';
import {
  setProductsData,
  removeProductsData,
} from '../../redux/actioncreators';
import { useDispatch, useSelector } from 'react-redux';
import {
  GET_PRODUCTS_SUCCESS,
  ERROR_GETTING_PRODUCTS,
} from '../../constants/api';
import API from '../../api/API';

export default function ProductsSection() {
  const [t, i18n] = useTranslation();
  const products = useSelector(state => state.productsData);
  const userData = useSelector(state => ({
    id: state.userData.id,
    activeProduct: state.userData.activeProduct,
  }));

  const dispatch = useDispatch();

  useEffect(() => {
    document.title = t('dashboardpage.productssection.tabTitle');
  });

  useEffect(() => {
    (async function getProducts() {
      const response = await API.fetchProductsData();
      switch (response.status) {
        case GET_PRODUCTS_SUCCESS:
          dispatch(setProductsData(response.products));
          return;
        case ERROR_GETTING_PRODUCTS:
          showToast(
            'error',
            t('dashboardpage.productssection.toasts.errorgettingcatalog')
          );
          return;
        default:
          return;
      }
    })();

    return () => dispatch(removeProductsData());
  }, []); //eslint-disable-line

  async function startPurchaseProcess(user, selectedProduct) {
    const answer = await question(
      t('dashboardpage.productssection.purchaseconfirm.text'),
      t('dashboardpage.productssection.purchaseconfirm.confirmbutton'),
      t('dashboardpage.productssection.purchaseconfirm.cancelbutton')
    );
    if (answer.isConfirmed) {
      API.purchaseProduct(user, selectedProduct);
      showToast(
        'success',
        t('dashboardpage.productssection.toasts.purchasecomplete')
      );
    } else {
      showToast(
        'error',
        t('dashboardpage.productssection.toasts.purchasecancelled')
      );
    }
  }

  function checkProductMatches(storeProduct) {
    if (userData.activeProduct) {
      if (
        userData.activeProduct.product.productIdentifier ===
        storeProduct.productIdentifier
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  function handleProductClick(storeProduct) {
    if (checkProductMatches(storeProduct)) {
      return null;
    } else {
      startPurchaseProcess(userData.id, storeProduct);
    }
  }

  return (
    <SectionLayout optClass="products-section">
      <SectionTitle>{t('dashboardpage.productssection.title')}</SectionTitle>
      <WidgetsBox layout="products">
        {products.length !== 0 ? (
          products.map(product => {
            if (product.isActive) {
              return (
                <Product
                  data={product}
                  key={product.productIdentifier}
                  onClick={() => handleProductClick(product)}
                  userHasProduct={checkProductMatches(product)}
                  period={
                    checkProductMatches(product) && {
                      begins: new Date(
                        userData.activeProduct.begin
                      ).toLocaleString(i18n.language),
                      ends: new Date(userData.activeProduct.end).toLocaleString(
                        i18n.language
                      ),
                    }
                  }
                />
              );
            } else {
              return null;
            }
          })
        ) : (
          <Spinner
            color="black"
            position="fixed"
            offset={{ top: '20px', right: '20px' }}
          />
        )}
      </WidgetsBox>
    </SectionLayout>
  );
}
