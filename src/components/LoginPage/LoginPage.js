import React, { useState, useEffect } from 'react';
//Components
import LoginForm from '../LoginForm/LoginForm';
import Spinner from '../Spinner/Spinner';
import { Redirect } from 'react-router-dom';
//Utils
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
//Styles
import './LoginPage.scss';

export default function LoginPage() {
  const userLoggedIn = useSelector(state => state.userLoginStatus);
  const [isLoggingIn, setIsLoggingIn] = useState(false);
  const [t] = useTranslation();

  useEffect(() => {
    document.title = t('loginpage.tabTitle');
  });

  return (
    <div className="login-page">
      {isLoggingIn && (
        <Spinner
          color="white"
          position="fixed"
          offset={{ top: '20px', right: '20px' }}
        />
      )}
      <LoginForm isLoggingIn={isLoggingIn} setIsLoggingIn={setIsLoggingIn} />
      {userLoggedIn && <Redirect to="/dashboard" />}
    </div>
  );
}
