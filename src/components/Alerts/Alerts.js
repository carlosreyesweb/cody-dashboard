import Swal from 'sweetalert2';
//Styles
import './Alerts.scss';

/**
 * Toast notification trigger.
 * @param {string} icon
 * @param {string} text
 */
export function toast(icon, text) {
  Swal.fire({
    icon,
    text,
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 4000,
    customClass: {
      popup: 'toast',
      content: 'toast__text',
      icon: 'toast__icon',
    },
  });
}

/**
 * Adaptive warning popup trigger.
 * @param {string} title
 * @param {string} text
 * @param {string} confirmButtonText
 */

export function warning(title, text, confirmButtonText) {
  Swal.fire({
    icon: 'warning',
    title,
    text,
    buttonsStyling: false,
    confirmButtonText,
    customClass: {
      confirmButton: 'confirm-button',
    },
    focusConfirm: false,
  });
}

/**
 * Adaptive question popup. When called, it returns a Promise with the
 * user's answer.
 * @param {string} text
 * @param {string} confirmButtonText
 * @param {string} cancelButtonText
 */
export function question(text, confirmButtonText, cancelButtonText) {
  return Swal.fire({
    icon: 'question',
    text,
    buttonsStyling: false,
    confirmButtonText,
    showCancelButton: true,
    cancelButtonText,
    customClass: {
      confirmButton: 'confirm-button',
      cancelButton: 'cancel-button',
    },
    focusConfirm: false,
  });
}
