import React from 'react';
//Utils
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
//Styles
import './ActiveProductWidget.scss';

/**
 * Wrapper widget for Product Component. It only adds a title to
 * identify active product in Products Section.
 */
export default function ActiveProductWidget({ children }) {
  const [t] = useTranslation();

  return (
    <div className="active-product-widget">
      <span className="active-product-widget__title">
        {t('dashboardpage.profilesection.activeproductwidget.title')}
      </span>
      {children}
    </div>
  );
}

ActiveProductWidget.propTypes = {
  children: PropTypes.element.isRequired,
};
