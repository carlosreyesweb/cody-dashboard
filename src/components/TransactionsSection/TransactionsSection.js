import React, { useEffect } from 'react';
//Components
import SectionLayout from '../../layout/SectionLayout/SectionLayout';
import SectionTitle from '../SectionTitle/SectionTitle';
//Utils
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
//Styles
import './TransactionsSection.scss';

export default function TransactionsSection() {
  const [t, i18n] = useTranslation();
  const transactions = useSelector(state => state.userData.transactions);

  useEffect(() => {
    document.title = t('dashboardpage.transactionssection.tabTitle');
  });

  return (
    <SectionLayout optClass="transactions-section">
      <SectionTitle>
        {t('dashboardpage.transactionssection.title')}
      </SectionTitle>
      <div className="transactions-table-wrapper">
        <table className="transactions-table">
          <thead className="transactions-table__heads">
            <tr>
              <th>
                {t('dashboardpage.transactionssection.table.productidcolumn')}
              </th>
              <th>{t('dashboardpage.transactionssection.table.datecolumn')}</th>
              <th>
                {t('dashboardpage.transactionssection.table.amountcolumn')}
              </th>
              <th>
                {t('dashboardpage.transactionssection.table.currencycolumn')}
              </th>
              <th>
                {t('dashboardpage.transactionssection.table.statuscolumn')}
              </th>
            </tr>
          </thead>
          <tbody className="transactions-table__body">
            {transactions.length !== 0 ? (
              transactions.map((transaction, index) => (
                <tr
                  className={
                    transaction.status === 'completed'
                      ? 'transaction-completed'
                      : transaction.status === 'pending'
                      ? 'transaction-pending'
                      : transaction.status === 'rejected'
                      ? 'transaction-rejected'
                      : ''
                  }
                  key={index}
                >
                  <td>{transaction.productIdentifier}</td>
                  <td>
                    {new Date(transaction.transactionDate).toLocaleString(
                      i18n.language
                    )}
                  </td>
                  <td>{transaction.amount}</td>
                  <td>{transaction.unit}</td>
                  <td>{transaction.status}</td>
                </tr>
              ))
            ) : (
              <tr className="no-transactions">
                <td colSpan="5">
                  {t('dashboardpage.transactionssection.table.noentriestext')}
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </SectionLayout>
  );
}
