import React from 'react';
//Utils
import PropTypes from 'prop-types';
//Styles
import './Form.scss';

export function InputText({ name, initialValue, changeHandler, placeHolder }) {
  return (
    <input
      name={name}
      className="input text"
      type="text"
      onChange={changeHandler}
      defaultValue={initialValue}
      placeholder={placeHolder}
      required
    />
  );
}

InputText.propTypes = {
  name: PropTypes.string.isRequired,
  initialValue: PropTypes.string,
  changeHandler: PropTypes.func.isRequired,
  placeHolder: PropTypes.string.isRequired,
};

export function InputEmail({ name, initialValue, changeHandler, placeHolder }) {
  return (
    <input
      className="input email"
      name={name}
      type="email"
      onChange={changeHandler}
      defaultValue={initialValue}
      placeholder={placeHolder}
      required
    />
  );
}

InputEmail.propTypes = {
  name: PropTypes.string.isRequired,
  initialValue: PropTypes.string,
  changeHandler: PropTypes.func.isRequired,
  placeHolder: PropTypes.string.isRequired,
};

export function InputPassword({
  name,
  initialValue,
  changeHandler,
  placeHolder,
}) {
  return (
    <input
      className="input password"
      name={name}
      type="password"
      onChange={changeHandler}
      defaultValue={initialValue}
      required
      placeholder={placeHolder}
    />
  );
}

InputPassword.propTypes = {
  name: PropTypes.string.isRequired,
  initialValue: PropTypes.string,
  changeHandler: PropTypes.func.isRequired,
  placeHolder: PropTypes.string.isRequired,
};

export function InputPhone({ name, initialValue, changeHandler, placeHolder }) {
  return (
    <input
      className="input phone"
      defaultValue={initialValue}
      onChange={changeHandler}
      placeholder={placeHolder}
      name={name}
      type="tel"
      pattern="^\+\d{1,3} \d{3} \d{3}-\d{4}"
      required
    />
  );
}

InputPhone.propTypes = {
  name: PropTypes.string.isRequired,
  initialValue: PropTypes.string,
  changeHandler: PropTypes.func.isRequired,
  placeHolder: PropTypes.string.isRequired,
};

export function Label({ forInput, children }) {
  return (
    <label htmlFor={forInput} className="label">
      {children}
    </label>
  );
}

Label.propTypes = {
  forInput: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default function Form({ submitFunction, title, children }) {
  return (
    <form onSubmit={submitFunction} className="form">
      <span className="form__title">{title}</span>
      {children}
    </form>
  );
}

Form.propTypes = {
  submitFunction: PropTypes.func,
  title: PropTypes.string.isRequired,
};
