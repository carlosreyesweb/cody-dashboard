import React, { useEffect } from 'react';
//Components
import SectionLayout from '../../layout/SectionLayout/SectionLayout';
import SectionTitle from '../SectionTitle/SectionTitle';
import WidgetsBox from '../../layout/WidgetsBox/WidgetsBox';
import DailyUsesChart from '../DailyUsesChart/DailyUsesChart';
import CategoryUsesChart from '../CategoryUsesChart/CategoryUsesChart';
//Utils
import { useTranslation } from 'react-i18next';

export default function MainSection() {
  const [t] = useTranslation();

  useEffect(() => {
    document.title = t('dashboardpage.mainsection.tabTitle');
  });

  return (
    <SectionLayout optClass="main-section">
      <SectionTitle>{t('dashboardpage.mainsection.title')}</SectionTitle>
      <WidgetsBox layout="graphs">
        <DailyUsesChart />
        <CategoryUsesChart />
      </WidgetsBox>
    </SectionLayout>
  );
}
