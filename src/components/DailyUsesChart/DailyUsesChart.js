import React from 'react';
//Components
import { Line } from 'react-chartjs-2';
import { ReactComponent as NoDataPic } from '../../assets/graphs-no-data.svg';
//Utils
import { useUsesData } from '../../hooks/hooks';
import { useTranslation } from 'react-i18next';
//Styles
import './DailyUsesChart.scss';

export default function DailyUsesChart() {
  const [t] = useTranslation();
  const data = useUsesData(
    'date',
    t('dashboardpage.mainsection.charts.dailyuses.label')
  );

  let options;

  if (data) {
    data.datasets[0] = {
      ...data.datasets[0],
      // Styles for the line in the chart.
      borderColor: '#0081ba',
      borderWidth: 5,
      fill: false,
    };
    // Reshaping data to show only last 5 days in chart
    data.labels = data.labels.slice(-5);
    data.datasets[0].data = data.datasets[0].data.slice(-5);

    options = {
      title: {
        text: t('dashboardpage.mainsection.charts.dailyuses.title'),
        display: true,
        fontSize: 20,
        fontFamily: 'Ubuntu',
        fontColor: '#0081ba',
      },
      responsive: true,
      maintainAspectRatio: false,
      // Hide grid form chart
      scales: {
        xAxes: [
          {
            gridLines: {
              display: false,
            },
          },
        ],
        yAxes: [
          {
            gridLines: {
              display: false,
            },
            ticks: {
              stepSize: 25,
            },
          },
        ],
      },
    };
  }

  return (
    <div className="daily-uses-chart">
      {data ? (
        <Line data={data} options={options} />
      ) : (
        <>
          <NoDataPic className="daily-uses-chart--nodata" />
          {t('dashboardpage.mainsection.charts.nodata')}
        </>
      )}
    </div>
  );
}
