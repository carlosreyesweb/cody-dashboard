import React from 'react';
//Components
import Form, {
  Label,
  InputText,
  InputEmail,
  InputPassword,
  InputPhone,
} from '../Form/Form';
import Button from '../Buttons/Button';
import { toast as showToast } from '../Alerts/Alerts';
//Utils
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { setUserData } from '../../redux/actioncreators';
import { useInputHandler } from '../../hooks/hooks';
import {
  USER_DATA_UPDATED,
  ERROR_UPDATING_USER_DATA,
} from '../../constants/api';
import API from '../../api/API';
import PropTypes from 'prop-types';

/**
 * Wrapper to inject user data and data update logic into
 * generic form.
 */
export default function ProfileForm({ isUpdatingData, setIsUpdatingData }) {
  const [t] = useTranslation();
  const userData = useSelector(state => ({
    id: state.userData.id,
    fullname: state.userData.fullname,
    email: state.userData.email,
    password: state.userData.password,
    phoneNumber: state.userData.phoneNumber,
  }));
  const dispatch = useDispatch();
  const [formData, handleChange] = useInputHandler(userData);

  async function updateUserData(event) {
    event.preventDefault();
    setIsUpdatingData(true);
    const response = await API.requestUserDataUpdate(userData.id, formData);
    switch (response.status) {
      case USER_DATA_UPDATED:
        dispatch(setUserData(response.newUserData));
        showToast(
          'success',
          t('dashboardpage.profilesection.toasts.successupdatingdata')
        );
        setIsUpdatingData(false);
        break;
      case ERROR_UPDATING_USER_DATA:
        showToast(
          'error',
          t('dashboardpage.profilesection.toasts.errorupdatingdata')
        );
        setIsUpdatingData(false);
        break;
      default:
        break;
    }
  }

  return (
    <div className="profile-form">
      <Form
        submitFunction={updateUserData}
        title={t('dashboardpage.profilesection.profileform.title')}
      >
        <Label forInput="fullname">
          {t('dashboardpage.profilesection.profileform.fullname.label')}
        </Label>
        <InputText
          name="fullname"
          initialValue={userData.fullname}
          changeHandler={handleChange}
          placeHolder={t(
            'dashboardpage.profilesection.profileform.fullname.placeholder'
          )}
        />
        <Label forInput="email">
          {t('dashboardpage.profilesection.profileform.email.label')}
        </Label>
        <InputEmail
          name="email"
          initialValue={userData.email}
          changeHandler={handleChange}
          placeHolder={t(
            'dashboardpage.profilesection.profileform.email.placeholder'
          )}
        />
        <Label forInput="password">
          {t('dashboardpage.profilesection.profileform.password.label')}
        </Label>
        <InputPassword
          name="password"
          initialValue={userData.password}
          changeHandler={handleChange}
          placeHolder={t(
            'dashboardpage.profilesection.profileform.password.placeholder'
          )}
        />
        <Label forInput="phoneNumber">
          {t('dashboardpage.profilesection.profileform.phonenumber.label')}
        </Label>
        <InputPhone
          name="phoneNumber"
          initialValue={userData.phoneNumber}
          changeHandler={handleChange}
          placeHolder={t(
            'dashboardpage.profilesection.profileform.phonenumber.placeholder'
          )}
        />
        <Button type="primary" disabled={isUpdatingData ? true : false}>
          {isUpdatingData
            ? t(
                'dashboardpage.profilesection.profileform.submitButton.updating'
              )
            : t('dashboardpage.profilesection.profileform.submitButton.update')}
        </Button>
      </Form>
    </div>
  );
}

ProfileForm.propTypes = {
  isUpdatingData: PropTypes.bool.isRequired,
  setIsUpdatingData: PropTypes.func.isRequired,
};
