import React from 'react';
//Components
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUserCircle,
  faTachometerAlt,
  faStore,
  faCreditCard,
  faPowerOff,
  faBars,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
//Utils
import { useTranslation } from 'react-i18next';
import { setLogoutState, removeUserData } from '../../redux/actioncreators';
import { useSelector, useDispatch } from 'react-redux';
import { useInteractiveMenu } from '../../hooks/hooks';
//Styles
import './Sidebar.scss';

export default function Sidebar() {
  const username = useSelector(state => state.userData.fullname);
  const dispatch = useDispatch();
  const [t] = useTranslation();
  const [isOpen, toggleShowSidebar] = useInteractiveMenu(768);

  function logout() {
    dispatch(setLogoutState());
    dispatch(removeUserData());
  }

  return (
    <>
      <button className="menu-button" onClick={toggleShowSidebar}>
        <FontAwesomeIcon icon={isOpen ? faTimes : faBars} />
      </button>
      <div
        className={`sidebar ${isOpen ? 'sidebar--shown' : 'sidebar--hidden'}`}
      >
        <NavLink
          to="/dashboard/profile"
          className="sidebar__profile-item"
          activeClassName="sidebar__item--active"
          onClick={toggleShowSidebar}
        >
          <FontAwesomeIcon icon={faUserCircle} className="sidebar__item-icon" />
          <span className="sidebar__profile-item--text">
            <span>{username}</span>
            <span>{t('sidebar.profilebutton')}</span>
          </span>
        </NavLink>

        <NavLink
          exact
          to="/dashboard"
          className="sidebar__item"
          activeClassName="sidebar__item--active"
          onClick={toggleShowSidebar}
        >
          <FontAwesomeIcon
            icon={faTachometerAlt}
            className="sidebar__item-icon"
          />
          {t('sidebar.dashboardbutton')}
        </NavLink>

        <NavLink
          exact
          to="/dashboard/products"
          className="sidebar__item"
          activeClassName="sidebar__item--active"
          onClick={toggleShowSidebar}
        >
          <FontAwesomeIcon icon={faStore} className="sidebar__item-icon" />
          {t('sidebar.productsbutton')}
        </NavLink>

        <NavLink
          exact
          to="/dashboard/transactions"
          className="sidebar__item"
          activeClassName="sidebar__item--active"
          onClick={toggleShowSidebar}
        >
          <FontAwesomeIcon icon={faCreditCard} className="sidebar__item-icon" />
          {t('sidebar.transactionsbutton')}
        </NavLink>

        <button className="sidebar__logout-button" onClick={logout}>
          <FontAwesomeIcon icon={faPowerOff} />
        </button>
      </div>
    </>
  );
}
