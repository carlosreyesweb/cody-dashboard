import React from 'react';
//Styles
import './SplashScreen.scss';

export default function SplashScreen({ children }) {
  return <div className="splash-screen">{children}</div>;
}
