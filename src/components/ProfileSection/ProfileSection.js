import React, { useState, useEffect } from 'react';
//Components
import SectionLayout from '../../layout/SectionLayout/SectionLayout';
import SectionTitle from '../SectionTitle/SectionTitle';
import WidgetsBox from '../../layout/WidgetsBox/WidgetsBox';
import ActiveProductWidget from '../ActiveProductWidget/ActiveProductWidget';
import Product from '../Product/Product';
import ProfileForm from '../ProfileForm/ProfileForm';
import Spinner from '../Spinner/Spinner';
//Utils
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

export default function ProfileSection() {
  const [t, i18n] = useTranslation();
  const activeProduct = useSelector(state => state.userData.activeProduct);
  const [isUpdatingData, setIsUpdatingData] = useState(false);

  useEffect(() => {
    document.title = t('dashboardpage.profilesection.tabTitle');
  });

  return (
    <SectionLayout optClass="profile-section">
      <SectionTitle>{t('dashboardpage.profilesection.title')}</SectionTitle>
      <WidgetsBox layout="profile">
        {activeProduct && (
          <ActiveProductWidget>
            <Product
              data={activeProduct.product}
              userHasProduct={true}
              period={{
                begins: new Date(activeProduct.begin).toLocaleString(
                  i18n.language
                ),
                ends: new Date(activeProduct.end).toLocaleString(i18n.language),
              }}
            />
          </ActiveProductWidget>
        )}
        <ProfileForm
          isUpdatingData={isUpdatingData}
          setIsUpdatingData={setIsUpdatingData}
        />
      </WidgetsBox>
      {isUpdatingData && (
        <Spinner
          position="fixed"
          color="black"
          offset={{ top: '20px', right: '20px' }}
        />
      )}
    </SectionLayout>
  );
}
