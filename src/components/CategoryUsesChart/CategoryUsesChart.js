import React from 'react';
//Components
import { Pie } from 'react-chartjs-2';
import { ReactComponent as NoDataPic } from '../../assets/graphs-no-data.svg';
//Utils
import { useUsesData } from '../../hooks/hooks';
import { useTranslation } from 'react-i18next';
//Styles
import './CategoryUsesChart.scss';

export default function CategoryUsesChart() {
  const [t] = useTranslation();
  const data = useUsesData('category', null);

  let options;

  if (data) {
    data.datasets[0] = {
      ...data.datasets[0],
      /** Color for each portion of the pie */
      backgroundColor: ['#0081ba', '#9c34eb', '#eb9c34'],
    };

    options = {
      title: {
        text: t('dashboardpage.mainsection.charts.categoryuses.title'),
        display: true,
        fontSize: 20,
        fontFamily: 'Ubuntu',
        fontColor: '#0081ba',
      },
      legend: {
        position: 'bottom',
      },
      responsive: true,
      maintainAspectRatio: false,
    };
  }

  return (
    <div className="category-uses-chart">
      {data ? (
        <Pie data={data} options={options} />
      ) : (
        <>
          <NoDataPic className="category-uses-chart--nodata" />
          {t('dashboardpage.mainsection.charts.nodata')}
        </>
      )}
    </div>
  );
}
