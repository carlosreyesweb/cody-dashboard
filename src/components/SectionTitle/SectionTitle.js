import React from 'react';
//Utils
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
//Styles
import './SectionTitle.scss';

export default function SectionTitle({ children }) {
  const [t] = useTranslation();
  return (
    <div className="section-title">
      <span>{t('dashboardpage.sectiontitle.currentlyviewing')}</span>
      <h1>{children}</h1>
    </div>
  );
}

SectionTitle.propTypes = {
  children: PropTypes.string.isRequired,
};
