import React from 'react';
//Components
import Form, { Label, InputEmail, InputPassword } from '../Form/Form';
import Button from '../Buttons/Button';
import { toast as showToast } from '../Alerts/Alerts';
//Utils
import { useTranslation } from 'react-i18next';
import {
  LOGIN_SUCCESS,
  WRONG_CREDENTIALS,
  ERROR_LOGIN,
} from '../../constants/api';
import API from '../../api/API';
import { setUserData, setLoginState } from '../../redux/actioncreators';
import { useDispatch } from 'react-redux';
import { useInputHandler } from '../../hooks/hooks';
import PropTypes from 'prop-types';
//Styles
import './LoginForm.scss';

/**
 * Wrapper to inject login and input capture logic to a <Form />
 * component
 */
export default function LoginForm({ setIsLoggingIn, isLoggingIn }) {
  const [t] = useTranslation();
  const dispatch = useDispatch();
  const [formData, handleChange] = useInputHandler({});

  async function login(event) {
    event.preventDefault();
    setIsLoggingIn(true);
    const response = await API.requestUserLogin(
      formData.email,
      formData.password
    );
    switch (response.status) {
      case LOGIN_SUCCESS:
        dispatch(setUserData(response.user));
        dispatch(setLoginState());
        break;
      case WRONG_CREDENTIALS:
        setIsLoggingIn(false);
        showToast('error', t('loginpage.toasts.wrongcredentials'));
        break;
      case ERROR_LOGIN:
        setIsLoggingIn(false);
        showToast('error', t('loginpage.toasts.unknownerror'));
        break;
      default:
        break;
    }
  }

  return (
    <div className="login-form">
      <Form submitFunction={login} title={t('loginpage.form.title')}>
        <Label forInput="email">{t('loginpage.form.email.label')}</Label>
        <InputEmail
          name="email"
          changeHandler={handleChange}
          placeHolder={t('loginpage.form.email.placeholder')}
        />
        <Label forInput="password">{t('loginpage.form.password.label')}</Label>
        <InputPassword
          name="password"
          changeHandler={handleChange}
          placeHolder={t('loginpage.form.password.placeholder')}
        />
        <Button type="primary" disabled={isLoggingIn && true}>
          {isLoggingIn
            ? t('loginpage.form.submitbutton.logging')
            : t('loginpage.form.submitbutton.login')}
        </Button>
      </Form>
    </div>
  );
}

LoginForm.propTypes = {
  isLoggingIn: PropTypes.bool.isRequired,
  setIsLoggingIn: PropTypes.func.isRequired,
};
