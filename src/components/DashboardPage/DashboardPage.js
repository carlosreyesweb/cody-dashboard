import React, { useEffect } from 'react';
//Components
import Sidebar from '../Sidebar/Sidebar';
import MainSection from '../MainSection/MainSection';
import ProfileSection from '../ProfileSection/ProfileSection';
import ProductsSection from '../ProductsSection/ProductsSection';
import TransactionsSection from '../TransactionsSection/TransactionsSection';
import { warning as showWarning } from '../Alerts/Alerts';
import { Route, useRouteMatch, Redirect } from 'react-router-dom';
//Utils
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
//Styles
import './DashboardPage.scss';

export default function DashboardPage() {
  const [t] = useTranslation();
  const dashboardBase = useRouteMatch('/dashboard');
  const userLoggedIn = useSelector(state => state.userLoginStatus);
  const userHasActiveProduct = useSelector(
    state => state.userData.activeProduct
  );

  useEffect(() => {
    if (!userHasActiveProduct) {
      showWarning(
        t('dashboardpage.alerts.noactiveproduct.title'),
        t('dashboardpage.alerts.noactiveproduct.text'),
        t('dashboardpage.alerts.noactiveproduct.buttontext')
      );
    }
  }, [userHasActiveProduct, t]);

  return (
    <div className="dashboard-page">
      <Sidebar />
      <Route exact path={dashboardBase.path}>
        <MainSection />
      </Route>
      <Route path={`${dashboardBase.path}/profile`}>
        <ProfileSection />
      </Route>
      <Route path={`${dashboardBase.path}/transactions`}>
        <TransactionsSection />
      </Route>
      <Route path={`${dashboardBase.path}/products`}>
        <ProductsSection />
      </Route>
      {userLoggedIn || <Redirect to="/" />}
    </div>
  );
}
