import React from 'react';
//Utils
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
//Styles
import './Product.scss';

export default function Product({ data, userHasProduct, onClick, period }) {
  const [t] = useTranslation();

  return (
    <div
      className={`product ${userHasProduct ? 'product--active' : ''}`}
      onClick={onClick}
    >
      <span className="product__name">{data.name}</span>
      <span className="product__id">{data.productIdentifier}</span>
      <p className="product__description">{data.description}</p>
      {userHasProduct && period ? (
        <>
          <span className="product__begin-date">
            {t('dashboardpage.productssection.product.beginsdate')}
          </span>
          <span>{period.begins}</span>
          <span className="product__end-date">
            {t('dashboardpage.productssection.product.endsdate')}
          </span>
          <span>{period.ends}</span>
        </>
      ) : null}
      <span className="product__price">
        {data.price ? `${data.price} ${data.currency}` : 'FREE'}
      </span>
      {userHasProduct ? (
        <span className="product__active-flag">
          {t('dashboardpage.productssection.product.activeflag')}
        </span>
      ) : (
        false
      )}
    </div>
  );
}

Product.propTypes = {
  data: PropTypes.shape({
    productIdentifier: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    currency: PropTypes.string.isRequired,
  }),
  userHasProduct: PropTypes.bool,
  onClick: PropTypes.func,
  period: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      begins: PropTypes.string.isRequired,
      ends: PropTypes.string.isRequired,
    }),
  ]),
};
