import React from 'react';
//Utils
import PropTypes from 'prop-types';
//Styles
import './Button.scss';

export default function Button({ type, children, onClick, disabled }) {
  return (
    <button onClick={onClick} className={`${type}-button`} disabled={disabled}>
      {children}
    </button>
  );
}

Button.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};
