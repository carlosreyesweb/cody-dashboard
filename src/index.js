import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
//Components
import SplashScreen from './components/SplashScreen/SplashScreen';
import Spinner from './components/Spinner/Spinner';
import App from './App';
//Utils
import { Provider } from 'react-redux';
import store from './redux/store';
import './i18n/i18next';
//Styles
import './styles/index.scss';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Suspense
        fallback={
          <SplashScreen>
            <Spinner />
          </SplashScreen>
        }
      >
        <App />
      </Suspense>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
