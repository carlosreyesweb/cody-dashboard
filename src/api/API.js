import * as constants from '../constants/api';

class API {
  /**
   *
   * @param {string} email
   * @param {string} password
   * @returns A promise with two possible objects: in case of resolve,
   * its property 'user' is an object with all of the user data.
   */
  async requestUserLogin(email, password) {
    try {
      const data = await (
        await fetch(
          `http://localhost:3004/users?email=${email}&password=${password}`
        )
      ).json();
      if (data.length !== 0) {
        const [user] = data;
        return { status: constants.LOGIN_SUCCESS, user };
      } else {
        return { status: constants.WRONG_CREDENTIALS };
      }
    } catch (error) {
      return { status: constants.ERROR_LOGIN };
    }
  }

  /**
   *
   * @param {number} userId
   * @param {object} data - An object with new user data provided in
   * profile form.
   * @returns A promise: in case of resolve, method will return an
   * object with updated user data object inside of it..
   */
  async requestUserDataUpdate(userId, data) {
    try {
      const newUserData = await (
        await fetch(`http://localhost:3004/users/${userId}`, {
          method: 'PATCH',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ ...data }),
        })
      ).json();
      return { status: constants.USER_DATA_UPDATED, newUserData };
    } catch (error) {
      return { status: constants.ERROR_UPDATING_USER_DATA };
    }
  }

  /**
   * @returns A promise: in case of resolve, method returns an object
   * with products array inside of it.
   */
  async fetchProductsData() {
    try {
      const products = await (
        await fetch(`http://localhost:3004/products`)
      ).json();
      return { status: constants.GET_PRODUCTS_SUCCESS, products };
    } catch (error) {
      return { status: constants.ERROR_GETTING_PRODUCTS };
    }
  }

  /**
   * Fake purchase product method.
   * @param {number} forUserId
   * @param {object} selectedProduct
   */
  purchaseProduct(forUserId, selectedProduct) {
    console.log(
      `User ID ${forUserId} has selected product "${selectedProduct.name}"`
    );
  }
}

export default new API();
